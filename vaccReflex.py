from agents import *

loc_A = (0, 0)
loc_B = (1, 0)


def SimpleReflexAgentProgram():
    """This agent takes action based solely on the percept. [Figure 2.10]"""

    def program(percept):
        loc, status = percept
        return ('Suck' if status == 'Dirty'
                else 'Right' if loc == loc_A
                else 'Left')

    return program


# Create a simple reflex agent the two-state environment
program = SimpleReflexAgentProgram()
simple_reflex_agent = Agent(program)

trivial_vacuum_env = TrivialVacuumEnvironment()
print("State of the Environment: {}.".format(trivial_vacuum_env.status))

trivial_vacuum_env.add_thing(simple_reflex_agent)
print("SimpleReflexVacuumAgent is located at {}.".format(simple_reflex_agent.location))

# Run the environment
trivial_vacuum_env.step()

# Check the current state of the environment
print("State of the Environment: {}.".format(trivial_vacuum_env.status))
print("SimpleReflexVacuumAgent is located at {}.".format(simple_reflex_agent.location))

# Run the environment
trivial_vacuum_env.step()

# Check the current state of the environment
print("State of the Environment: {}.".format(trivial_vacuum_env.status))
print("SimpleReflexVacuumAgent is located at {}.".format(simple_reflex_agent.location))

