from agents import *
from enum import Enum


class Dirt(Thing):
    pass


class House(Environment):

    def percept(self, agent):
        things = self.list_things_at(agent.location)
        return things

    def execute_action(self, agent, action):
        if action == "move":
            print('{} decided to {} at location: {}'.format(str(agent)[1:-1], action, agent.location))
            agent.move()
        elif action == "clean":
            items = self.list_things_at(agent.location, tclass=Dirt)
            if len(items) != 0:
                if agent.clean(items[0]):  # Have the vacuum
                    print('{} clean {} at location: {}'
                          .format(str(agent)[1:-1], str(items[0])[1:-1], agent.location))
                    self.delete_thing(items[0])  # Delete it from the Park after.

    def is_done(self):
        no_edibles = not any(isinstance(thing, Dirt) for thing in self.things)
        dead_agents = not any(agent.is_alive() for agent in self.agents)
        return dead_agents or no_edibles


class Position(Enum):
    LEFT = 0
    RIGHT = 1


class Vacuum(Agent):
    location = Position.LEFT  # 0 Left, 1 Rigth

    def move(self):
        if self.location == Position.LEFT:
            self.location = Position.RIGHT
        else:
            self.location = Position.LEFT

    def clean(thing):
        if isinstance(thing, Dirt):
            return True
        return False


def program(percepts):
    for p in percepts:
        if isinstance(p, Dirt):
            return 'clean'
    return 'move'


house = House()
vac = Vacuum(program)
dirt = Dirt()
house.add_thing(vac,  Position.LEFT)
house.add_thing(dirt,  Position.RIGHT)

print("Vacuum starts at Left, lets see if he can clean all!")
house.run(5)
#poner puntaje
#cambiar para que el ambiente no sea el que da los movimientos
