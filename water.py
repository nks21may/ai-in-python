from agents import *
from search import *


class Water(Problem):

    def value(self, state):
        return 1

    def actions(self, state):
        seq = []
        if state[0] < 4:
            seq.append("Fill 0")
        if state[1] < 3:
            seq.append("Fill 1")
        if state[0] > 0:
            seq.append("Drop 0")
        if state[1] > 0:
            seq.append("Drop 1")
        if state[0] > 0:
            seq.append("Swap 0")
        if state[1] > 0:
            seq.append("Swap 1")
        return seq

    def result(self, state, action):
        x = state[0]
        y = state[1]
        if action == "Fill 0":
            x = 4
        if action == "Fill 1":
            y = 3
        if action == "Drop 0":
            x = 0
        if action == "Drop 1":
            y = 0
        if action == "Swap 0":
            y = state[0] + state[1]
            x = 0
            if y > 3:
                x = y - 3
                y = 3
        if action == "Swap 1":
            x = state[1] + x
            y = 0
            if x > 4:
                y = x - 4
                x = 4
        k = (x, y)
        print(self.goal)
        if is_in(k, self.goal):
            print("***********************")
        print(state, " ", action, " ", k)
        return k


w = Water((0, 0), (2, 0))
n = breadth_first_tree_search(w)
print(n.solution)
